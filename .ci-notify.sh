#!/bin/bash

TIME="10"
URL="https://api.telegram.org/bot$TELEGRAM_BOT_TOKEN/sendMessage"
TEXT="Commit status: $1%0A%0AProject:+$CI_PROJECT_NAME%0A%0AURL:+$CI_PROJECT_URL/pipelines/$CI_PIPELINE_ID/%0A%0ABranch:+$CI_COMMIT_REF_SLUG%0A%0ACommit message: +$CI_COMMIT_MESSAGE%0A%0ACoverage report:+$CI_PAGES_URL%0A%0ACommiter:+$GITLAB_USER_NAME"

curl -s --max-time $TIME -d "chat_id=$TELEGRAM_CHAT_ID&disable_web_page_preview=1&text=$TEXT" $URL > /dev/null
